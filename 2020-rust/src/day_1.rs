use std::fs;
use itertools::Itertools;

pub fn run() {

    let filename = "input_1.txt";

    // println!("In file {}", filename);

    let contents = fs::read_to_string(filename)
        .expect("Something went wrong reading the file");

    // println!("With text:\n{}\n\n\n", contents);

    let numbers_1 = contents.lines().map(|number| { number.parse::<i32>().unwrap() });
    let numbers_2 = contents.lines().map(|number| { number.parse::<i32>().unwrap() });

    let tuples_2 = (numbers_1).cartesian_product(numbers_2);

    for (a, b) in tuples_2 {

        if a < b && a + b == 2020 {
            println!("{} * {} = {}", a, b, a*b);
        }
    }

    // let numbers_1 = contents.lines().map(|number| { number.parse::<i32>().unwrap() }).collect();
    // let numbers_2 = contents.lines().map(|number| { number.parse::<i32>().unwrap() }).collect();
    // let numbers_3 = contents.lines().map(|number| { number.parse::<i32>().unwrap() }).collect();

    let tuples : Vec<Vec<i32>> = vec![
        contents.lines().map(|number| { number.parse::<i32>().unwrap() }).collect(),
        contents.lines().map(|number| { number.parse::<i32>().unwrap() }).collect(),
        contents.lines().map(|number| { number.parse::<i32>().unwrap() }).collect(),
    ];

    let tuples_3 = tuples.iter().multi_cartesian_product();

    for v in tuples_3 {
        let a = v[0];
        let b = v[1];
        let c = v[2];
        if a < b && b < c && a + b + c == 2020 {
            println!("{} * {} * {} = {}", a, b, c, a*b*c);
        }
    }
}

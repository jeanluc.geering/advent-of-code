use std::env;
#[macro_use] extern crate lazy_static;

mod day_1;
mod day_2;
mod day_3;
mod day_4;

fn main() {
    let args: Vec<String> = env::args().collect();

    match &args[..] {
        [_, number] => day(number),
        _ => println!("which day?"),
    }
}

fn day(number: &String) {
    match number.as_ref() {
        "1" => day_1::run(),
        "2" => day_2::run(),
        "3" => day_3::run(),
        "4" => day_4::run(),

        _ => println!("which day?"),
    }
}

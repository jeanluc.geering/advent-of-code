use std::fs;
use regex::Regex;

fn is_valid_1(line: &str) -> bool {
    lazy_static! {
        static ref RE: Regex = Regex::new(r"^(\d+)-(\d+) ([a-z]): ([a-z]+)$").unwrap();
    }
    let caps = RE.captures(line).unwrap();

    let min = caps.get(1).unwrap().as_str().parse::<usize>().unwrap();
    let max = caps.get(2).unwrap().as_str().parse::<usize>().unwrap();
    let letter = caps.get(3).unwrap().as_str();
    let letters = caps.get(4).unwrap().as_str();

    let count = letters.matches(letter).count();

    let valid = min <= count && count <= max;

    // println!("{} - {} <= {} <= {} {} in {}", valid, min, count, max, letter, letters);

    valid
}

fn is_valid_2(line: &str) -> bool {
    lazy_static! {
        static ref RE: Regex = Regex::new(r"^(\d+)-(\d+) ([a-z]): ([a-z]+)$").unwrap();
    }
    let caps = RE.captures(line).unwrap();

    let min = caps.get(1).unwrap().as_str().parse::<usize>().unwrap();
    let max = caps.get(2).unwrap().as_str().parse::<usize>().unwrap();
    let letter = caps.get(3).unwrap().as_str().chars().next().unwrap();
    let letters: Vec<char> = caps.get(4).unwrap().as_str().chars().collect();

    
    let valid = (letter == letters[min-1]) ^ (letter == letters[max-1]);

    println!("{} in {} or {} - {}", letter, letters[min-1], letters[max-1], valid);

    valid
}

pub fn run() {
    let contents = fs::read_to_string("input_2.txt")
        .expect("Something went wrong reading the file");

    let _valid_lines = contents.lines().filter(|line| is_valid_1(line)).count();
    // println!("valid lines first part: {}", valid_lines);

    let valid_lines = contents.lines().filter(|line| is_valid_2(line)).count();
    println!("valid lines second part: {}", valid_lines);
}
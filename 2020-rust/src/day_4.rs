use std::fs;

pub fn run() {
    // let filename = "input_4_example.txt";
    let filename = "input_4.txt";

    let contents = fs::read_to_string(filename)
        .expect("Something went wrong reading the file");

    let passports : Vec<Vec<&str>> = contents.split("\n\n").map(|lines| {
        lines.split_whitespace().collect()
    }).collect();

    // println!("{:#?}", passports)

    let required = [
        "byr",
        "iyr",
        "eyr",
        "hgt",
        "hcl",
        "ecl",
        "pid",
    ];
    let count = passports.iter().filter(|passport| {
        let keys: Vec<&str> = passport.iter().map(|kv| &kv[..3]).collect();

        let valid = required.iter().all(|e| keys.contains(e));
        // println!("{:?} - {}", passport, valid);
        valid
    }).count();

    println!("valid passports (part 1): {}", count);
}

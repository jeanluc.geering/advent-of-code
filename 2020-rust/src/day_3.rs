use std::fs;

fn map_line(line: &str) -> Vec<i32> {
    line.chars().map(|char|
        if char == '#' {
            1
        }
        else {
            0
        }
    ).collect()
}

fn trees(data: &Vec<Vec<i32>>, slope_x: usize, slope_y: usize) -> i32 {
    let height = data.len();
    let width = data[0].len();

    let rounds = height / slope_x;

    let result = (0..rounds).map(|i| {
        let x = slope_x * i;
        let y = slope_y * i;
        let tree = data[x][y % width];
        // println!("coordinates: ({}:{}) - {}", x, y, tree);
        tree
    }).sum();

    println!("result right {} down {} => {}", slope_y, slope_x, result);

    result
}

pub fn run() {
    // let filename = "input_3_example.txt";
    let filename = "input_3.txt";

    let contents = fs::read_to_string(filename)
        .expect("Something went wrong reading the file");

    let data: Vec<Vec<i32>> = contents.lines().map(|line| map_line(line)).collect();
    let height = data.len();
    let width = data[0].len();

    // println!("map:");
    // for line in &data {
    //     println!("{:?}", line);
    // }
    println!("map size: {} x {}", width, height);

    let result_1 = trees(&data, 1, 1);
    let result_2 = trees(&data, 1, 3);
    let result_3 = trees(&data, 1, 5);
    let result_4 = trees(&data, 1, 7);
    let result_5 = trees(&data, 2, 1);

    let result = result_1 * result_2 * result_3 * result_4 * result_5;

    println!("result: {}", result);
}

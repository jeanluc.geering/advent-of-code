defmodule AoC do
  def read_line(file) do
    file
    |> File.read!()
    |> String.trim()
    |> String.to_charlist()
    |> Enum.map(fn
      ?( -> 1
      ?) -> -1
    end)
  end

  def part_1(input) do
    level = input
    |> read_line()
    |> Enum.sum()

    IO.puts("day_1 - part_1 - #{input} => #{level}")
  end

  def part_2(input) do
    position = input
    |> read_line()
    |> Enum.reduce_while({0, 0}, fn
      -1, {0, position} -> {:halt, position + 1}
      change, {level, position} -> {:cont, {level + change, position + 1}}
    end)

    IO.puts("day_1 - part_2 - #{input} => #{position}")
  end
end

AoC.part_1("example.txt")
AoC.part_1("input.txt")
AoC.part_2("example.txt")
AoC.part_2("input.txt")

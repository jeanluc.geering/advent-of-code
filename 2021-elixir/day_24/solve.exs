defmodule AoC do

  def run(instructions, input) do
    vars = {0,0,0,0}
    state = {vars, input}

    Enum.reduce(instructions, state, &exec/2)
  end

  defp var_index(:w), do: 0
  defp var_index(:x), do: 1
  defp var_index(:y), do: 2
  defp var_index(:z), do: 3

  defp get_var(vars, name), do: elem(vars, var_index(name))
  defp put_var(vars, name, value), do: put_elem(vars, var_index(name), value)

  defp get_var_or_int(_, n) when is_integer(n), do: n
  defp get_var_or_int(vars, n) when is_atom(n), do: get_var(vars, n)

  defp op(:add, v1, v2), do: v1 + v2
  defp op(:mul, v1, v2), do: v1 * v2
  defp op(:div, v1, v2), do: div(v1, v2)
  defp op(:mod, v1, v2), do: rem(v1, v2)
  defp op(:eql, v1, v1), do: 1
  defp op(:eql, _, _), do: 0
  defp op(:neq, v1, v1), do: 0
  defp op(:neq, _, _), do: 1

  defp exec({:inp, a}, {vars, [i | input]}) do
    {
      put_var(vars, a, i),
      input
    }
  end

  defp exec({:set, a, b}, {vars, input}) when is_integer(b) do
    {
      put_var(vars, a, b),
      input
    }
  end

  defp exec({:inspect}, {vars, input}) do
    IO.inspect(vars)
    {vars, input}
  end

  defp exec({op, a, b}, {vars, input}) do
    v1 = get_var(vars, a)
    v2 = get_var_or_int(vars, b)

    {
      put_var(vars, a, op(op, v1, v2)),
      input
    }
  end


  def parse(filename) do
    filename
    |> File.stream!()
    |> Stream.reject(&(&1 == "\n"))
    |> Stream.reject(&String.starts_with?(&1, "#"))
    |> Enum.map(fn line ->
      line
      |> String.trim("\n")
      |> String.split(" ")
      |> then(fn
        ["inspect"] -> {:inspect}
        ["inp", var | _] -> {:inp, String.to_existing_atom(var)}
        [op, var, var_or_int | _] -> {String.to_existing_atom(op), String.to_existing_atom(var), parse_var_or_int(var_or_int)}
      end)
    end)
  end

  defp parse_var_or_int("w"), do: :w
  defp parse_var_or_int("x"), do: :x
  defp parse_var_or_int("y"), do: :y
  defp parse_var_or_int("z"), do: :z
  defp parse_var_or_int(n), do: String.to_integer(n)

  def parse_and_run(filename, input) do
    filename
    |> parse()
    |> run(input)
  end

  defp is_monad(digits, instructions) when is_list(digits) and length(digits) == 14 do
    {{_, _, _, z}, []} = run(instructions, digits)
    case digits do
      [_,_,_,_,_,_,_,_,_,9,9,9,9,9] -> IO.puts("#{Integer.undigits(digits)} - #{z}")
      _ -> :skip
    end

    z == 0
  end

  defp is_monad(digits, instructions) when is_list(digits) and length(digits) == 10 do
    {{_, _, _, z}, []} = run(instructions, digits)

    z == 0
  end

  def part_1() do
    # instructions = parse("input.txt")
    instructions = parse("opt_2.txt")

    Stream.iterate(9999999999, &(&1 - 1))
    |> Stream.map(&Integer.digits/1)
    |> Stream.reject(fn digits -> Enum.any?(digits, &(&1 == 0)) end)
    |> Enum.reduce_while(false, fn digits, _ ->
      if is_monad(digits, instructions) do
        {:halt, Integer.undigits(digits ++ [9,3,8,9])}
      else
        {:cont, false}
      end
    end)
    |> IO.inspect()

    # is_monad(instructions, [9,9,9,9,9,9,9,9,9,9,9,9,9,9])
    # is_monad(instructions, Integer.digits(13579246899999))

    # result = 0

    # IO.puts("day_16 - part_1 - #{filename} - #{result}")
  end

end

# example_1 = [
#   {:inp, :x},
#   {:mul, :x, -1},
# ]
# example_2 = [
#   {:inp, :z},
#   {:inp, :x},
#   {:mul, :z, 3},
#   {:eql, :z, :x}
# ]

# AoC.run(example_1, [8]) |> IO.inspect()
# AoC.run(example_2, [1,2]) |> IO.inspect()
# AoC.run(example_2, [1,3]) |> IO.inspect()

# AoC.parse("example.txt") |> IO.inspect()
# AoC.parse("input.txt") |> IO.inspect()

# AoC.parse_and_run("example.txt", [1]) |> IO.inspect()
# AoC.parse_and_run("example.txt", [2]) |> IO.inspect()
# AoC.parse_and_run("example.txt", [3]) |> IO.inspect()
# AoC.parse_and_run("example.txt", [4]) |> IO.inspect()
# AoC.parse_and_run("example.txt", [5]) |> IO.inspect()

# AoC.parse_and_run("input.txt", Integer.digits(99999999999999)) |> IO.inspect()
# AoC.parse_and_run("opt_1.txt", Integer.digits(99999999999999)) |> IO.inspect()

# AoC.parse_and_run("input.txt", Integer.digits(12345678912345)) |> IO.inspect()
# AoC.parse_and_run("opt_1.txt", Integer.digits(12345678912345)) |> IO.inspect()

# AoC.parse_and_run("input.txt", Integer.digits(91919191919191)) |> IO.inspect()
# AoC.parse_and_run("opt_1.txt", Integer.digits(91919191919191)) |> IO.inspect()

AoC.parse_and_run("input.txt", Integer.digits(99999999999389)) |> IO.inspect()
AoC.parse_and_run("input.txt", Integer.digits(99929999999389)) |> IO.inspect()
AoC.parse_and_run("input.txt", Integer.digits(99929995999389)) |> IO.inspect()
AoC.parse_and_run("input.txt", Integer.digits(94929995999389)) |> IO.inspect()
AoC.parse_and_run("input.txt", Integer.digits(74929995999389)) |> IO.inspect()
AoC.parse_and_run("opt_1.txt", Integer.digits(74929995999389)) |> IO.inspect()

AoC.parse_and_run("input.txt", Integer.digits(11118151637112)) |> IO.inspect()

# AoC.part_1()

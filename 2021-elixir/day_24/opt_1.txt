inp w (w1)
add z w
add z 14

inp w (w2)
mul z 26
add z w
add z 8

inp w (w3)
mul z 26
add z w
add z 4

inp w (w4)
mul z 26
add z w
add z 10 (z % 26 == w4 + 10)

inp w (w5)
set x 0
add x z
mod x 26 
add x -3 (x == w4 + 7)
neq x w
div z 26
set y 25
mul y x
add y 1 (y is 1 or 26)
mul z y
set y 14
add y w
mul y x
add z y

inp w (w6)
set x 0
add x z
mod x 26
add x -4
neq x w
div z 26
set y 25
mul y x
add y 1 (y is 1 or 26)
mul z y
set y 10
add y w
mul y x
add z y

inp w (w7)
mul z 26
add z w
add z 4

inp w (w8)
set x 0
add x z
mod x 26
add x -8
neq x w
div z 26
set y 25
mul y x
add y 1 (y is 1 or 26)
mul z y
set y 14
add y w
mul y x
add z y

inp w (w9)
set x 0
add x z
mod x 26
add x -3
neq x w
div z 26
set y 25
mul y x
add y 1 (y is 1 or 26)
mul z y
set y 1
add y w
mul y x
add z y

inp w (w10)
set x 0
add x z
mod x 26
add x -12
neq x w
div z 26
set y 25
mul y x
add y 1 (y is 1 or 26)
mul z y
set y 6
add y w
mul y x
add z y

inp w (w11)
mul z 26
add z w

inp w (w12)
set x 0
add x z
mod x 26
add x -6
neq x w
div z 26
set y 25
mul y x
add y 1 (y is 1 or 26)
mul z y
set y 9
add y w
mul y x
add z y

# if w14 == w13 + 1 => stop here

inspect

inp w (w13)
mul z 26
add z w
add z 13

inp w (w14)
set x 0
add x z
mod x 26
add x -12 (x == w13 + 1)
neq x w (w13 + 1 =?= w14)
div z 26
set y 25
mul y x
add y 1 (y is 1 or 26)
mul z y
set y 12
add y w
mul y x (y is 12w or 0)
add z y

# if w14 == w13 + 1
# div z 26

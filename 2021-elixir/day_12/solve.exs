defmodule AoC do

  def parse_input(filename) do
    filename
    |> File.stream!()
    |> Enum.flat_map(fn line ->
      tunnel = line
      |> String.trim("\n")
      |> String.split("-")

      [
        tunnel,
        tunnel |> Enum.reverse(),
      ]
    end)
    |> Enum.filter(fn
      # never go back to start
      [_, "start"] -> false
      _ -> true
    end)
    |> Enum.group_by(&Enum.at(&1, 0), &Enum.at(&1, 1))
    |> Map.delete("end")
  end

  def part_1(filename) do
    tunnels = parse_input(filename)

    result = traverse(tunnels, &visit_1/2, ["start"])
    # |> IO.inspect()
    |> Enum.count()

    IO.puts("day_12 - part_1 - #{filename} - #{result}")
  end

  defp is_big?(cave), do: Regex.match?(~r/^[A-Z]/, cave)

  defp traverse(_, _, ["end" | _] = path) do
    [Enum.reverse(path)]
  end

  defp traverse(tunnels, visit, [cave | _] = path) do
    tunnels
    |> Map.get(cave)
    |> Enum.flat_map(fn target ->
      if visit.(path, target) do
        traverse(tunnels, visit, [target | path])
      else
        []
      end
    end)
  end

  defp visit_1(path, cave) do
    cond do
      is_big?(cave) ->
        # big cave, always visit
        true
      Enum.member?(path, cave) ->
        # small cave, already visited
        false
      true ->
        # small cave, not yet visited
        true
    end
  end

  defp has_duplicates?(list) do
    list
    |> Enum.reduce_while(%MapSet{}, fn x, acc ->
      if MapSet.member?(acc, x), do: {:halt, nil}, else: {:cont, MapSet.put(acc, x)}
    end)
    |> is_nil()
  end

  defp double_small?(path) do
    path
    |> Enum.reject(&is_big?/1)
    |> has_duplicates?()
  end

  defp visit_2(path, cave) do
    cond do
      is_big?(cave) ->
        # big cave, always visit
        true
      Enum.member?(path, cave) and double_small?(path) ->
        # small cave, already visited, double visit already done
        false
      true ->
        # small cave, not yet visited, or only visited 1x
        true
    end
  end

  def part_2(filename) do
    tunnels = parse_input(filename)

    result = traverse(tunnels, &visit_2/2, ["start"])
    # |> IO.inspect()
    |> Enum.count()

    IO.puts("day_12 - part_2 - #{filename} - #{result}")
  end

end

AoC.part_1("example_1.txt")
AoC.part_1("example_2.txt")
AoC.part_1("example_3.txt")
AoC.part_1("input.txt")
AoC.part_2("example_1.txt")
AoC.part_2("example_2.txt")
AoC.part_2("example_3.txt")
AoC.part_2("input.txt")

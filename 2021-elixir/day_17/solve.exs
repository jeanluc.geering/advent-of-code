defmodule AoC do

  def part_1(target_x, target_y) do

    target = {target_x, target_y}

    speed_x = speed_x(target_x)
    max_y = max_y(target_y)

    result = 1..max_y
    |> Enum.map(fn speed_y -> pos_stream(speed_x,speed_y,target_y) end)
    |> Enum.filter(fn positions -> target_hit?(positions, target) end)
    |> Enum.map(fn positions -> max_height(positions) end)
    |> Enum.max()

    IO.puts("day_17 - part_1 - #{inspect(target)} - #{result}")
  end

  defp speed_x(x1.._) do
    x1 * 2
    |> :math.sqrt()
    |> trunc()
  end
  defp max_y(y1.._) do
    abs(y1)
  end

  defp x_stream(init), do: Stream.unfold({init, 0}, fn {speed, pos} ->
    pos = pos + speed
    speed = case speed do
      0 -> 0
      s when s > 0 -> s-1
      s when s < 0 -> s+1
    end
    {pos, {speed, pos}}
  end)
  defp y_stream(init), do: Stream.unfold({init, 0}, fn {speed, pos} ->
    pos = pos + speed
    speed = speed - 1
    {pos, {speed, pos}}
  end)

  defp pos_stream(init_x, init_y, min_y.._) do
    Stream.zip(x_stream(init_x), y_stream(init_y))
    |> Enum.take_while(fn {_x, y} -> y >= min_y end)
  end

  defp target_hit?(positions, {target_x, target_y}) do
    Enum.any?(positions, fn {x,y} -> x in target_x and y in target_y end)
  end

  defp max_height(positions) do
    Enum.reduce(positions, 0, fn {_x, y}, acc ->
      max(y, acc)
    end)
  end

end

AoC.part_1(20..30, -10..-5)
AoC.part_1(50..193, -136..-86)

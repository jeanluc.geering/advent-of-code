defmodule AoC do

  defp parse_input(filename) do
    {dots, folds} = filename
    |> File.read!()
    |> String.trim("\n")
    |> String.split("\n")
    |> Enum.split_while(&(&1 != ""))

    dots = dots
    |> Enum.map(fn line ->
      line
      |> String.split(",")
      |> Enum.map(&String.to_integer/1)
      |> List.to_tuple()
    end)

    folds = folds
    |> Enum.drop(1)
    |> Enum.map(fn line ->
      line
      |> String.split(["fold along ", "="], trim: true)
    end)
    |> Enum.map(fn [axis,index] ->
      {
        String.to_atom(axis),
        String.to_integer(index)
      }
    end)

    {dots, folds}
  end

  def part_1(filename) do
    {dots, folds} = parse_input(filename)

    # print(dots)

    result = folds
    |> Enum.take(1)
    |> Enum.reduce(dots, &fold/2)
    |> Enum.count()

    IO.puts("day_13 - part_1 - #{filename} - #{result}")
  end

  defp print(dots) do
    width = dots
    |> Enum.map(&elem(&1, 0))
    |> Enum.max()

    grid = dots
    |> Enum.group_by(
      &elem(&1, 1),
      fn {x, _} -> {x, "#"} end
    )

    height = grid
    |> Map.keys()
    |> Enum.max()

    for y <- 0..height do
      for x <- 0..width do
        grid
        |> Map.get(y, [])
        |> Enum.into(%{})
        |> Map.get(x, ".")
      end
      |> IO.puts()
    end

    IO.puts("")

    dots
  end

  defp fold_point({:x,index}, {x,y}) when x < index,
    do: {x,y}
  defp fold_point({:x,index}, {x,y}),
    do: {2*index-x, y}
  defp fold_point({:y,index}, {x,y}) when y < index,
    do: {x,y}
  defp fold_point({:y,index}, {x,y}),
    do: {x, 2*index-y}

  defp fold(fold, dots) do
    dots
    |> Enum.map(&fold_point(fold, &1))
    |> Enum.uniq()
    # |> print()
  end

  def part_2(filename) do
    {dots, folds} = parse_input(filename)

    IO.puts("day_13 - part_2 - #{filename}\n")

    folds
    |> Enum.reduce(dots, &fold/2)
    |> print()
  end

end

AoC.part_1("example.txt")
AoC.part_1("input.txt")
AoC.part_2("example.txt")
AoC.part_2("input.txt")

defmodule AoC do

  defp parse_input(filename) do
    filename
  end

  def part_1(filename) do
    _ = parse_input(filename)
    |> IO.inspect()

    result = 0

    IO.puts("day_20 - part_1 - #{filename} - #{result}")
  end

end

AoC.part_1("example.txt")
# AoC.part_1("input.txt")
# AoC.part_2("example.txt")
# AoC.part_2("input.txt")

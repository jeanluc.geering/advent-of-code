defmodule AoC do

  defp parse_input(filename, truncate) do
    filename
    |> File.stream!()
    |> Enum.map(fn line ->
      line
      |> String.trim("\n")
      |> String.graphemes()
      |> Enum.map(&String.to_integer/1)
      |> Enum.take(truncate)
    end)
    |> Enum.take(truncate)
    # |> IO.inspect()
  end

  def part_1(filename) do
    levels = parse_input(filename, 3)

    size = length(levels) - 1

    start = %{
      pos: {0,0},
      end: size,
      # path: [{0,0}],
      cost: 0,
      levels: levels,
    }

    result = find_path(start, %{})
    |> Map.get({size, size})

    # upper_bound_h = levels
    # |> List.first()
    # |> Enum.drop(1)
    # |> Enum.sum()

    # upper_bound_v = levels
    # |> Enum.drop(1)
    # |> Enum.map(&List.last/1)
    # |> Enum.sum()

    # upper_bound = upper_bound_h + upper_bound_v

    IO.puts("day_15 - part_1 - #{filename} - #{result}")
  end

  defp neighbours(%{pos: {i, j}, end: e}) do
    [
      {i-1,j},
      {i,j+1},
      {i+1,j},
      {i,j-1},
    ]
    |> Enum.reject(fn {i,j} ->
      i < 0 or j < 0 or i > e or j > e
    end)
  end

  defp go_to(%{levels: levels} = state, {i,j} = pos) do
    risk = levels |> Enum.at(i) |> Enum.at(j)
    cost = state.cost + risk
    %{
      state |
      pos: pos,
      cost: cost,
    }
  end

  defp better?(%{pos: pos, cost: current_cost}, acc) do
    previous_cost = Map.get(acc, pos, :inf)

    current_cost < previous_cost
  end

  defp find_path(%{pos: {e, e} = pos, end: e} = state, acc) do
    if better?(state, acc) do
      IO.puts("arrived - new solution: #{state.cost}")
      Map.put(acc, pos, state.cost)
    else
      acc
    end
  end

  defp find_path(%{pos: pos} = state, acc) do
    # IO.puts("pos: #{inspect(pos)} - #{inspect(acc)}")
    if better?(state, acc) do
      acc = Map.put(acc, pos, state.cost)

      state
      |> neighbours()
      |> Enum.map(&go_to(state, &1))
      |> Enum.reduce(acc, &find_path/2)
    else
      # IO.puts("pos: #{inspect(pos)} - worse path - skip")
      acc
    end
  end

end

AoC.part_1("example.txt")
# AoC.part_1("input.txt")
# AoC.part_2("example.txt")
# AoC.part_2("input.txt")

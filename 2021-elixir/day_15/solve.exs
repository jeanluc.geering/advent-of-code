defmodule AoC do

  defp parse_input(filename, truncate) do
    filename
    |> File.stream!()
    |> Enum.map(fn line ->
      line
      |> String.trim("\n")
      |> String.graphemes()
      |> Enum.map(&String.to_integer/1)
      |> Enum.take(truncate)
    end)
    |> Enum.take(truncate)
    # |> IO.inspect()
  end

  def solve(rows) do
    size = length(rows) - 1
    target = {size, size}
    levels = rows
    |> Enum.with_index()
    |> Enum.flat_map(fn {cols, i} ->
      cols
      |> Enum.with_index()
      |> Enum.map(fn {level, j} ->
        {{i,j},level}
      end)
    end)
    |> Enum.into(%{})

    reached = %{
      {0,0} => 0
    }

    1..10000
    |> Enum.reduce_while(reached, fn distance, reached ->
      # if rem(distance, 10) == 0 do
      #   IO.puts("exploring #{distance} - reached #{map_size(reached)}")
      # end

      new_points = reached
      |> Enum.filter(fn {_, start_cost} -> start_cost + 9 >= distance end)
      |> Enum.flat_map(fn {pos, start_cost} ->
        pos
        |> neighbours(size)
        |> Enum.reject(&Map.has_key?(reached, &1))
        |> Enum.map(fn pos ->
          risk = Map.get(levels, pos)
          {pos, start_cost + risk}
        end)
        |> Enum.filter(fn {_, dest_cost} ->
          dest_cost == distance
        end)
      end)
      |> Enum.into(%{})

      case Map.get(new_points, target) do
        nil -> {:cont, Map.merge(reached, new_points)}
        cost -> {:halt, cost}
      end
    end)

  end

  defp neighbours({i, j}, e) do
    [
      {i-1,j},
      {i,j+1},
      {i+1,j},
      {i,j-1},
    ]
    |> Enum.reject(fn {i,j} ->
      i < 0 or j < 0 or i > e or j > e
    end)
  end

  def part_1(filename) do
    result = filename
    |> parse_input(100)
    |> solve()

    IO.puts("day_15 - part_1 - #{filename} - #{result}")
  end

  def part_2(filename) do
    result = filename
    |> parse_input(100)
    |> resize()
    |> solve()

    IO.puts("day_15 - part_2 - #{filename} - #{result}")
  end

  defp resize(rows) do
    long_rows = rows
    |> Enum.map(fn cols ->
      Enum.flat_map(0..4, &add(cols, &1))
    end)

    Enum.flat_map(0..4, fn x ->
      Enum.map(long_rows, &add(&1, x))
    end)
    # |> tap(fn rows ->
    #   rows
    #   |> Enum.map(&Enum.join/1)
    #   |> Enum.map(&IO.puts/1)
    # end)
  end

  defp add(values, 0), do: values
  defp add(values, x), do: Enum.map(values, &(rem(&1 - 1 + x, 9) + 1))

end

AoC.part_1("example.txt")
AoC.part_1("input.txt")
AoC.part_2("example.txt")
AoC.part_2("input.txt")

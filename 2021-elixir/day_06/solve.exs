defmodule AoC do

  def parse_input(filename) do
    filename
    |> File.read!()
    |> String.trim("\n")
    |> String.split(",")
    |> Enum.map(&String.to_integer/1)
  end

  def to_buckets(data) do
    data = Enum.frequencies(data)
    for i <- 0..8, do: Map.get(data, i, 0)
  end

  def next_day(buckets) do
    Enum.map(0..8, fn
      8 -> Enum.at(buckets, 0)
      6 -> Enum.at(buckets, 0) + Enum.at(buckets, 7)
      i -> Enum.at(buckets, i+1)
    end)
  end

  def count(input, days) do
    Enum.reduce(1..days, input, fn _, buckets ->
      next_day(buckets)
    end)
    |> Enum.sum()
  end

  def part_1(filename) do
    count = parse_input(filename)
    |> to_buckets()
    |> count(80)

    IO.puts("day_6 - part_1 - #{filename} - #{count}")
  end

  def part_2(filename) do
    count = parse_input(filename)
    |> to_buckets()
    |> count(256)

    IO.puts("day_6 - part_2 - #{filename} - #{count}")
  end

end

AoC.part_1("example.txt")
AoC.part_1("input.txt")
AoC.part_2("example.txt")
AoC.part_2("input.txt")

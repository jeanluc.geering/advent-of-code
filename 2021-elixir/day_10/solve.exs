defmodule AoC do

  def parse_input(filename) do
    filename
    |> File.stream!()
    |> Enum.map(fn line ->
      line
      |> String.trim("\n")
      |> String.graphemes()
    end)
  end

  def part_1(filename) do
    result = parse_input(filename)
    |> Enum.map(&find_bad_closer(&1, []))
    |> Enum.map(&score_1/1)
    |> Enum.sum()

    IO.puts("day_10 - part_1 - #{filename} - #{result}")
  end

  def open_2_close("("), do: ")"
  def open_2_close("["), do: "]"
  def open_2_close("{"), do: "}"
  def open_2_close("<"), do: ">"

  def score_1(")"), do: 3
  def score_1("]"), do: 57
  def score_1("}"), do: 1197
  def score_1(">"), do: 25137
  def score_1(_), do: 0

  def find_bad_closer([open|input], stack) when open in ~w"[ < ( {" do
    close = open_2_close(open)
    find_bad_closer(input, [close|stack])
  end
  def find_bad_closer([close|input], [close|stack]) do
    find_bad_closer(input, stack)
  end
  def find_bad_closer([close|_], _) do
    close
  end
  def find_bad_closer([], _) do
    nil
  end

  def part_2(filename) do
    scores = parse_input(filename)
    |> Enum.map(&close_seq(&1, []))
    |> Enum.reject(&(&1 == []))
    |> Enum.map(&score_seq/1)
    |> Enum.sort()

    pos = scores
    |> length()
    |> div(2)

    result = Enum.at(scores, pos)

    IO.puts("day_10 - part_2 - #{filename} - #{result}")
  end

  def close_seq([open|input], stack) when open in ~w"[ < ( {" do
    close = open_2_close(open)
    close_seq(input, [close|stack])
  end
  def close_seq([close|input], [close|stack]) do
    close_seq(input, stack)
  end
  def close_seq([], stack) do
    stack
  end
  def close_seq([_|_], _) do
    []
  end

  def score_2(")"), do: 1
  def score_2("]"), do: 2
  def score_2("}"), do: 3
  def score_2(">"), do: 4

  def score_seq(seq) do
    Enum.reduce(seq, 0, fn char, acc ->
      (acc * 5) + score_2(char)
    end)
  end

end

AoC.part_1("example.txt")
AoC.part_1("input.txt")
AoC.part_2("example.txt")
AoC.part_2("input.txt")

defmodule AoC do

  defp parse_input(filename) do
    grid = filename
    |> File.read!()
    |> String.split("\n")
    |> Enum.map(fn line ->
      line
      |> String.graphemes()
    end)

    h = grid |> length()
    w = grid |> List.first() |> length()

    dim = {h, w}

    cucumbers = grid
    |> Enum.with_index()
    |> Enum.flat_map(fn {row, i} ->
      row
      |> Enum.with_index()
      |> Enum.map(fn
        {".", _} -> nil
        {">", j} -> {{i, j}, :hor}
        {"v", j} -> {{i, j}, :ver}
      end)
    end)
    |> List.flatten()
    |> Enum.reject(&is_nil/1)
    |> Enum.into(%{})

    {cucumbers, dim}
  end

  defp print({grid, {h, w}, _, _} = state) do
    border = String.duplicate("=", w)
    IO.puts(border)
    for i <- 0..h-1 do
      for j <- 0..w-1 do
        case Map.get(grid, {i, j}) do
          nil -> "."
          :hor -> ">"
          :ver -> "v"
        end
      end
      |> IO.puts()
    end
    IO.puts(border)

    state
  end

  defp filter(grid, direction) do
    grid
    |> Enum.filter(fn
      {_, ^direction} -> true
      _ -> false
    end)
    |> Enum.map(&elem(&1, 0))
  end

  defp step({_, _, [], []}, i), do: i
  # defp step(_, 2), do: 2

  defp step(state, i) do
    IO.puts("STEP #{i+1}")
    state
    |> step_hor()
    |> step_ver()
    # |> print()
    |> step(i+1)
  end

  defp step_hor({initial_grid, dim, hor, ver}) do
    acc = {initial_grid, dim, [], ver}
    Enum.reduce(hor, acc, fn pos, {next_grid, dim, hor, ver} = state ->
      next = right(pos, dim)
      case Map.get(initial_grid, next) do
        nil ->
          next_grid = next_grid
          |> Map.put(pos, nil)
          |> Map.put(next, :hor)

          {next_grid, dim, [next|hor], ver} |> add_candidates(pos, :ver_first)
        _ ->
          state
      end
    end)
  end
  defp step_ver({initial_grid, dim, hor, ver}) do
    acc = {initial_grid, dim, hor, []}
    Enum.reduce(ver, acc, fn pos, {next_grid, dim, hor, ver} = state ->
      next = down(pos, dim)
      case Map.get(initial_grid, next) do
        nil ->
          next_grid = next_grid
          |> Map.put(pos, nil)
          |> Map.put(next, :ver)

          {next_grid, dim, hor, [next|ver]} |> add_candidates(pos, :hor_first)
        _ ->
          state
      end
    end)
  end

  defp right({i, j}, {_, w}) do
    r = rem(j+1, w)
    {i,r}
  end
  defp down({i, j}, {h, _}) do
    d = rem(i+1, h)
    {d,j}
  end
  defp left({i, j}, {_, w}) do
    l = if j-1 < 0 do
      j-1+w
    else
      j-1
    end
    {i,l}
  end
  defp up({i, j}, {h, _}) do
    u = if i-1 < 0 do
      i-1+h
    else
      i-1
    end
    {u,j}
  end

  defp add_candidates({grid, dim, hor, ver}, pos, :hor_first) do
    cond do
      Map.get(grid, left(pos, dim)) == :hor ->
        {grid, dim, [left(pos, dim)|hor], ver}
      Map.get(grid, up(pos, dim)) == :ver ->
        {grid, dim, hor, [up(pos, dim)|ver]}
      true ->
        {grid, dim, hor, ver}
    end
  end

  defp add_candidates({grid, dim, hor, ver}, pos, :ver_first) do
    cond do
      Map.get(grid, up(pos, dim)) == :ver ->
        {grid, dim, hor, [up(pos, dim)|ver]}
      Map.get(grid, left(pos, dim)) == :hor ->
        {grid, dim, [left(pos, dim)|hor], ver}
      true ->
        {grid, dim, hor, ver}
    end
  end

  def part_1(filename) do
    {grid, dim} = parse_input(filename)
    hor = filter(grid, :hor)
    ver = filter(grid, :ver)
    start = {grid, dim, hor, ver}
    |> print()

    result = step(start, 0)

    IO.puts("day_25 - part_1 - #{filename} - #{result}")
  end

end

# AoC.part_1("example.txt")
AoC.part_1("input.txt")
# AoC.part_2("example.txt")
# AoC.part_2("input.txt")

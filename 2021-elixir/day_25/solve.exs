defmodule AoC do

  defp parse_input(filename) do
    filename
    |> File.read!()
    |> String.split("\n")
    |> Enum.reject(&(&1 == ""))
    |> Enum.map(fn line ->
      line
      |> String.graphemes()
      |> Enum.map(fn
        "." -> 0
        ">" -> 1
        "v" -> -1
      end)
    end)
  end

  def print({grid, continue}), do: {print(grid), continue}
  def print(grid) do
    w = grid |> List.first() |> length()
    border = String.duplicate("=", w)
    IO.puts(border)
    Enum.each(grid, fn row ->
      row
      |> Enum.map(fn
        0 -> "."
        1 -> ">"
        -1 -> "v"
      end)
      |> IO.puts()
    end)
    IO.puts(border)

    grid
  end

  defp transpose({grid, continue}), do: {transpose(grid), continue}
  defp transpose(rows) do
    rows
    |> List.zip
    |> Enum.map(fn tuple ->
      tuple
      |> Tuple.to_list()
      |> Enum.map(&(&1 * -1))
    end)
  end

  defp step({_, false}, i), do: i
  # defp step(_, 2), do: 2
  defp step({grid, true}, i) do
    # IO.puts("STEP #{i+1}")

    {grid, false}
    |> move_right()
    |> transpose()
    |> move_right()
    |> transpose()
    # |> print()
    |> step(i+1)
  end

  defp move_right({grid, continue}) do
    {reversed_grid, continue} = grid
    |> Enum.reduce({[], continue}, fn row, {acc, continue} ->
      first = List.first(row)
      last = List.last(row)

      {reversed_row, continue} = [last | row]
      |> Enum.chunk_every(3, 1, [first])
      |> Enum.reduce({[], continue}, fn
        [_, 1, 0], {acc, _} -> {[0|acc], true}
        [1, 0, _], {acc, _} -> {[1|acc], true}
        [_, v, _], {acc, cont} -> {[v|acc], cont}
      end)

      row = Enum.reverse(reversed_row)
      {[row | acc], continue}
    end)

    {Enum.reverse(reversed_grid), continue}
  end

  def part_1(filename) do
    grid = parse_input(filename)

    # print(grid)

    result = step({grid, true}, 0)

    IO.puts("day_25 - part_1 - #{filename} - #{result}")
  end

end

# AoC.part_1("example.txt")
AoC.part_1("input.txt")
# AoC.part_2("example.txt")
# AoC.part_2("input.txt")

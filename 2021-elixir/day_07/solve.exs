defmodule AoC do

  def parse_input(filename) do
    filename
    |> File.read!()
    |> String.trim("\n")
    |> String.split(",")
    |> Enum.map(&String.to_integer/1)
    |> to_buckets()
  end

  def to_buckets(data) do
    max = Enum.max(data)
    data = Enum.frequencies(data)
    for i <- 0..max, do: Map.get(data, i, 0)
  end

  def part_1(filename) do
    buckets = parse_input(filename)

    l2r = buckets
    # |> IO.inspect()
    |> Enum.scan({0, 0}, fn i, {fuel, subs} -> {fuel+subs, subs+i} end)
    |> Enum.map(&elem(&1, 0))
    # |> IO.inspect()

    r2l = buckets
    |> Enum.reverse()
    |> Enum.scan({0, 0}, fn i, {fuel, subs} -> {fuel+subs, subs+i} end)
    |> Enum.map(&elem(&1, 0))
    |> Enum.reverse()
    # |> IO.inspect()

    count = [l2r, r2l]
    |> Enum.zip_with(&Enum.sum/1)
    |> Enum.min()
    # |> IO.inspect()

    IO.puts("day_7 - part_1 - #{filename} - #{count}")
  end

  def part_2(filename) do
    buckets = parse_input(filename)

    # buckets = [1,0,0,0,0,0,0,0,0,0,0,0]
    # buckets = [0,0,0,1,0,1,0,0,0,0,0,0,0,0]

    l2r = buckets
    # |> IO.inspect()
    |> Enum.scan({0, 0, 0}, fn
      i, {fuel, rate, subs} -> {fuel+rate, rate+subs+i, subs+i}
    end)
    |> Enum.map(&elem(&1, 0))
    # |> IO.inspect()

    r2l = buckets
    |> Enum.reverse()
    |> Enum.scan({0, 0, 0}, fn
      i, {fuel, rate, subs} -> {fuel+rate, rate+subs+i, subs+i}
    end)
    |> Enum.map(&elem(&1, 0))
    |> Enum.reverse()
    # |> IO.inspect()

    count = [l2r, r2l]
    |> Enum.zip_with(&Enum.sum/1)
    |> Enum.min()

    IO.puts("day_7 - part_2 - #{filename} - #{count}")
  end

end

AoC.part_1("example.txt")
AoC.part_1("input.txt")
AoC.part_2("example.txt")
AoC.part_2("input.txt")

defmodule AoC do
  def read_lines(file), do: file |> File.stream!() |> Stream.map(&String.trim/1)

  def part_1(input) do
    {position, depth} = input
    |> read_lines()
    |> Stream.map(fn
      "forward " <> value -> {String.to_integer(value), 0}
      "down " <> value -> {0, String.to_integer(value)}
      "up " <> value -> {0, -1 * String.to_integer(value)}
    end)
    |> Enum.reduce(fn {a, b}, {c, d} -> {a+c, b+d} end)

    IO.puts("day_2 - part_1 - #{input} - #{position * depth}")
  end

  def part_2(input) do
    {position, depth, _aim} = input
    |> read_lines()
    |> Stream.map(fn
      "forward " <> value -> {String.to_integer(value), 0}
      "down " <> value -> {0, String.to_integer(value)}
      "up " <> value -> {0, -1 * String.to_integer(value)}
    end)
    |> Enum.reduce({0, 0, 0}, fn
      {0, x}, {position, depth, aim} -> {position, depth, aim + x}
      {x, 0}, {position, depth, aim} -> {position + x, depth + (aim * x), aim}
    end)

    IO.puts("day_2 - part_2 - #{input} - #{position * depth}")
  end
end

AoC.part_1("example.txt")
AoC.part_1("input.txt")
AoC.part_2("example.txt")
AoC.part_2("input.txt")

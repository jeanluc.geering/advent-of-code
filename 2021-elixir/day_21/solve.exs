defmodule AoC do

  defp next(100), do: 1
  defp next(i), do: i + 1

  defp roll_3_times(dice) do
    Enum.reduce(1..3, {dice, 0}, fn _, {i, sum} ->
      val = next(i)
      {val, sum + val}
    end)
  end

  defp move(player, move) do
    next_pos = rem(player.pos + move, 10)

    %{player|pos: next_pos, score: player.score + next_pos + 1}
  end

  def play_1(is_player_1, players, dice, roll_count) do

    {dice, move} = roll_3_times(dice)

    players = update_in(players, [is_player_1], fn player ->
      move(player, move)
    end)

    if get_in(players, [is_player_1, :score]) >= 1000 do
      (roll_count + 3) * get_in(players, [!is_player_1, :score])
    else
      play_1(!is_player_1, players, dice, roll_count + 3)
    end
  end

  defp players(pos_1, pos_2) do
    %{
      true => %{
        name: "player 1",
        score: 0,
        pos: pos_1 - 1,
      },
      false => %{
        name: "player 2",
        score: 0,
        pos: pos_2 - 1,
      },
    }
  end

  def part_1(pos_1, pos_2) do
    result = play_1(true, players(pos_1, pos_2), 0, 0)
    IO.puts("day_21 - part_1 - {#{pos_1}, #{pos_2}} - #{result}")
  end

  defp dirac_dice() do
    sides = 3
    for i <- 1..sides, j <- 1..sides, k <- 1..sides do
      Enum.sum([i,j,k])
    end
    |> Enum.group_by(&(&1))
    |> Enum.map(fn {k,v} -> {k,length(v)} end)
  end

  defp play_2(is_player_1, players) do
    for {move, count} <- dirac_dice() do
      players = update_in(players, [is_player_1], fn player ->
        move(player, move)
      end)
      players
      |> evaluate_universe(is_player_1)
      |> Enum.map(&(&1 * count))
      |> List.to_tuple()
    end
    |> Enum.unzip()
    |> Tuple.to_list()
    |> Enum.map(&Enum.sum/1)
  end

  defp evaluate_universe(players, is_player_1) do
    if get_in(players, [is_player_1, :score]) >= 21 do
      if is_player_1 do
        [1, 0]
      else
        [0, 1]
      end
    else
      play_2(!is_player_1, players)
    end
  end

  def part_2(pos_1, pos_2) do
    play_2(true, players(pos_1, pos_2)) |> IO.inspect()
  end
end

# AoC.part_1(4, 8)
# AoC.part_1(8, 6)
# AoC.part_2(4, 8)
AoC.part_2(8, 6)

# 1,1,1 3
# 1,1,2 4
# 1,1,3 5
# 1,2,1 4
# 1,2,2 5
# 1,2,3 6

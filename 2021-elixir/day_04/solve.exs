defmodule AoC do

  def parse_input(filename) do
    [numbers, "" | boards] = File.read!(filename) |> String.split("\n")

    numbers = numbers
    |> String.split(",")
    |> Enum.map(&String.to_integer/1)

    boards = boards
    |> Enum.reject(&(&1 == ""))
    |> Enum.map(fn line ->
      line
      |> String.split()
      |> Enum.map(&String.to_integer/1)
    end)
    |> Enum.chunk_every(5)

    {numbers, boards}
  end

  def transpose(rows) do
    rows
    |> List.zip
    |> Enum.map(&Tuple.to_list/1)
  end

  def rows_and_cols(board) do
    board
    |> Enum.concat(transpose(board))
    |> Enum.map(&({&1, []}))
  end

  def part_1(filename) do
    {numbers, boards} = parse_input(filename)

    boards = Enum.map(boards, &rows_and_cols/1)

    {number, board} = Enum.reduce_while(numbers, boards, &step_1/2)

    sum = sum_unmarked(board)

    IO.puts("day_4 - part_1 - #{filename} - #{number * sum}")
  end

  def step_1(number, boards) do
    boards = boards |> Enum.map(&mark_number(&1, number))
    winner? = boards
    |> Enum.map(&check_board/1)
    |> Enum.any?()

    if (winner?) do
      {:halt, {number, Enum.find(boards, &check_board/1)}}
    else
      {:cont, boards}
    end
  end

  def mark_number(rows_and_cols, number) do
    rows_and_cols
    |> Enum.map(fn {unmarked, marked} ->
      if Enum.member?(unmarked, number) do
        {Enum.reject(unmarked, &(&1 == number)), [number|marked]}
      else
        {unmarked, marked}
      end
    end)
  end

  def check_board(rows_and_cols) do
    Enum.any?(rows_and_cols, fn
      {[], _} -> true
      _ -> false
    end)
  end

  def sum_unmarked(rows_and_cols) do
    rows_and_cols
    |> Enum.flat_map(fn {unmarked, _} -> unmarked end)
    |> Enum.sum()
    |> div(2)
  end

  def part_2(filename) do
    {numbers, boards} = parse_input(filename)

    boards = Enum.map(boards, &rows_and_cols/1)

    {number, board} = Enum.reduce_while(numbers, boards, &step_2/2)

    sum = sum_unmarked(board)

    IO.puts("day_4 - part_2 - #{filename} - #{number * sum}")
  end

  def step_2(number, boards) do
    remaining = boards
    |> Enum.map(&mark_number(&1, number))
    |> Enum.reject(&check_board/1)

    case remaining do
      [] ->
        [last] = boards
        {:halt, {number, mark_number(last, number)}}
      _ -> {:cont, remaining}
    end
  end
end

AoC.part_1("example.txt")
AoC.part_1("input.txt")
AoC.part_2("example.txt")
AoC.part_2("input.txt")

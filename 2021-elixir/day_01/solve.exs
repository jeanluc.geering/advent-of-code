defmodule AoC do
  def read_lines(file), do: file |> File.stream!() |> Stream.map(&String.trim/1)

  def part_1(input) do
    count = input
    |> read_lines()
    |> Stream.map(&String.to_integer/1)
    |> Stream.chunk_every(2, 1, :discard)
    # |> Stream.map(&IO.inspect/1)
    |> Stream.map(fn [a, b] -> a < b end)
    # |> Stream.map(&IO.inspect/1)
    |> Stream.filter(&(&1))
    # |> Stream.map(&IO.inspect/1)
    |> Enum.count()

    IO.puts("day_1 - part_1 - #{input} - #{count}")
  end

  def part_2(input) do
    count = input
    |> read_lines()
    |> Stream.map(&String.to_integer/1)
    |> Stream.chunk_every(3, 1, :discard)
    # |> Stream.map(&IO.inspect/1)
    |> Stream.map(&Enum.sum/1)
    |> Stream.chunk_every(2, 1, :discard)
    # |> Stream.map(&IO.inspect/1)
    |> Stream.map(fn [a, b] -> a < b end)
    # |> Stream.map(&IO.inspect/1)
    |> Stream.filter(&(&1))
    # |> Stream.map(&IO.inspect/1)
    |> Enum.count()

    IO.puts("day_1 - part_2 - #{input} - #{count}")
  end
end

AoC.part_1("example.txt")
AoC.part_1("input.txt")
AoC.part_2("example.txt")
AoC.part_2("input.txt")

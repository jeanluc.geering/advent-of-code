defmodule AoC do

  defp parse_input(filename) do
    filename
    |> File.read!()
    |> String.split("\n")
    |> Enum.reject(&(&1 == ""))
    |> Enum.map(fn line ->
      [on_off, box] = String.split(line, " ")

      box = box
      |> String.split(["x=","y=","z=","..",","], trim: true)
      |> Enum.map(&String.to_integer/1)
      |> Enum.chunk_every(2)
      |> Enum.map(fn [first, last] -> Range.new(first, last) end)

      status = case on_off do
        "on" -> 1
        "off" -> 0
      end

      {box, status}
    end)
  end

  def part_1(filename) do
    result = parse_input(filename)
    |> Enum.reject(fn {box, _} ->
      Enum.any?(box, fn range -> Range.disjoint?(range, -50..50) end)
    end)
    |> Enum.flat_map(&to_points/1)
    |> Enum.into(%{})
    |> Map.values()
    |> Enum.sum()

    IO.puts("day_22 - part_1 - #{filename} - #{result}")
  end

  defp to_points({[x_range, y_range, z_range], status}) do
    for x <- x_range, y <- y_range, z <- z_range do
      {{x, y, z}, status}
    end
  end

  def part_2(filename) do
    result = parse_input(filename)
    |> Enum.flat_map(&to_points/1)
    |> Enum.into(%{})
    |> Map.values()
    |> Enum.sum()

    IO.puts("day_22 - part_2 - #{filename} - #{result}")
  end

end

# AoC.part_1("example.txt")
# AoC.part_1("input.txt")
AoC.part_2("example_2.txt")
# AoC.part_2("input.txt")

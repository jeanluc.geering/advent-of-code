defmodule AoC do

  defp parse_input(filename) do
    filename
    |> File.stream!()
    |> Enum.map(fn line ->
      line
      |> String.trim("\n")
      |> String.graphemes()
      |> Enum.map(&String.to_integer/1)
    end)
  end

  defp to_grid(input) do
    input
    |> Enum.with_index()
    |> Enum.flat_map(fn {row, i} ->
      for {value, j} <- Enum.with_index(row) do
        {{i,j},value}
      end
    end)
    |> Enum.into(%{})
  end

  defp print(grid, info) when is_list(grid), do: grid |> Enum.into(%{}) |> print(info)
  defp print(grid, info) do
    IO.puts("#{info}")
    for i <- 0..9 do
      for j <- 0..9 do
        case Map.get(grid, {i, j}) do
          0 -> IO.ANSI.format([:green, "0"])
          x when x > 9 -> IO.ANSI.format([:red, "X"])
          d -> to_string(d)
        end
      end
    end
    |> Enum.join("\n")
    |> IO.puts()

    grid
  end

  def part_1(filename) do
    grid = parse_input(filename)
    |> to_grid()
    |> print("input")

    {_grid, result} = 1..100
    |> Enum.reduce({grid, 0}, &step/2)

    IO.puts("day_11 - part_1 - #{filename} - #{result}")
  end

  defp step(step, {grid, counter}) do
    grid = grid
    |> Enum.map(fn {k,v} -> {k,v+1} end)
    |> Enum.into(%{})
    |> print("==========\nstep: #{step}\nstart count: #{counter}")
    |> count_flashes(counter)
  end

  def high(grid) do
    grid
    |> Enum.filter(fn {_, v} -> v > 9 end)
    |> Enum.map(fn {k, _} -> k end)
  end

  defp count_flashes(grid, counter) do
    count_flashes(grid, counter, high(grid))
  end

  defp count_flashes(grid, counter, []) do
    case high(grid) do
      [] ->
        print(grid, "end count: #{counter}")
        {grid, counter}
      cells ->
        count_flashes(grid, counter, cells)
    end
  end
  defp count_flashes(grid, counter, [cell|rest]) do
    counter = counter + 1
    grid = grid |> Map.put(cell, 0)

    grid = cell
    |> neighbours()
    |> Enum.reduce(grid, fn cell, grid ->
      Map.update!(grid, cell, fn
        0 -> 0 # cell already flashed this round
        v -> v + 1
      end)
    end)
    # |> print("count: #{counter}")
    |> count_flashes(counter, rest)
  end

  defp neighbours({i,j}) do
    for i <- neighbours(i) do
      for j <- neighbours(j) do
        {i,j}
      end
    end
    |> List.flatten()
    |> Enum.reject(&(&1 == {i,j}))
  end
  defp neighbours(0), do: [0, 1]
  defp neighbours(9), do: [8, 9]
  defp neighbours(x), do: [x-1, x, x+1]

  def part_2(filename) do
    grid = parse_input(filename)
    |> to_grid()
    |> print("input")

    result = Enum.reduce_while(1..1000, grid, fn i, grid ->
      {grid, _} = step(i, {grid, 0})
      case grid |> Map.values() |> Enum.sum() do
        0 -> {:halt, i}
        _ -> {:cont, grid}
      end
    end)

    IO.puts("day_11 - part_2 - #{filename} - #{result}")
  end

end

AoC.part_1("example.txt")
AoC.part_1("input.txt")
AoC.part_2("example.txt")
AoC.part_2("input.txt")

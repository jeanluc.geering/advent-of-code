defmodule AoC do

  def parse_input(filename, inc \\ 0) do
    filename
    |> File.stream!()
    |> Enum.map(fn line ->
      line
      |> String.trim("\n")
      |> String.graphemes()
      # |> Enum.map(&String.to_integer/1)
      |> Enum.map(&(String.to_integer(&1) + inc))
    end)
  end

  def local_minima(grid) do
    for {row, i} <- Enum.with_index(grid) do
      for {val, j} <- Enum.with_index(row) do
        cond do
          val >= lookup(grid, i, j-1, 11) -> 0
          val >= lookup(grid, i, j+1, 11) -> 0
          val >= lookup(grid, i-1, j, 11) -> 0
          val >= lookup(grid, i+1, j, 11) -> 0
          true -> val
        end
      end
    end
  end

  def part_1(filename) do
    grid = parse_input(filename, 1)

    sum = grid
    |> local_minima()
    # |> print_grid()
    |> List.flatten()
    |> Enum.sum()

    IO.puts("day_9 - part_1 - #{filename} - #{sum}")
  end

  def print_grid(grid) do
    grid
    |> Enum.map(&Enum.join/1)
    |> Enum.join("\n")
    |> IO.puts()

    grid
  end

  def lookup(grid, i, j, fallback \\ nil)

  def lookup(_, -1, _, fallback), do: fallback
  def lookup(_, _, -1, fallback), do: fallback
  def lookup(grid, i, j, fallback) do
    grid
    |> Enum.at(i, [])
    |> Enum.at(j, fallback)
  end

  def part_2(filename) do
    grid = parse_input(filename)
    # |> print_acc(9)

    width = grid |> List.first() |> length()

    minima = grid
    |> local_minima()
    |> List.flatten()
    |> Enum.map(&(&1 > 0))
    |> Enum.chunk_every(width)

    acc = grid
    |> Enum.map(&Enum.map(&1, fn
      _ -> 0
    end))

    product = Enum.reduce(8..0, acc, fn level, acc ->
      acc
      |> down_to(grid, level, minima)
      # |> print_acc()
    end)
    |> List.flatten()
    |> Enum.sort()
    |> Enum.reverse()
    |> Enum.take(3)
    |> Enum.product()

    IO.puts("day_9 - part_2 - #{filename} - #{product}")
  end

  def down_to(acc, grid, threshold, minima) do
    for {row, i} <- Enum.with_index(acc) do
      for {val, j} <- Enum.with_index(row) do
        case lookup(grid, i, j) do
          ^threshold -> 1 + pull_around(acc, grid, threshold, i, j)
          level when level > threshold ->
            cond do
              val == 0 ->
                0
              get_in(minima, [Access.at!(i), Access.at!(j)]) ->
                val
              lookup(grid, i-1, j) == threshold ->
                0
              lookup(grid, i, j+1) == threshold ->
                0
              lookup(grid, i+1, j) == threshold ->
                0
              lookup(grid, i, j-1) == threshold ->
                0
              true ->
                val
            end
          _ -> val
        end
      end
    end
  end

  def pull_around(acc, grid, level, i, j) do
    top = if Enum.all?([
      # up diag (up l and up r)
      lookup(grid, i-1, j-1) != level,
      lookup(grid, i-1, j+1) != level,
      # opposite (above up)
      lookup(grid, i-2, j) != level,
    ]) do
      lookup(acc, i-1, j, 0)
    else
      0
    end

    left = if Enum.all?([
      # up left
      lookup(grid, i-1, j-1) != level,
      # opposite (left of left)
      lookup(grid, i, j-2) != level,
    ]) do
      lookup(acc, i, j-1, 0)
    else
      0
    end

    # up right
    right = if lookup(grid, i-1, j+1) != level do
      lookup(acc, i, j+1, 0)
    else
      0
    end

    bottom = lookup(acc, i+1, j, 0)

    Enum.sum([
      top,
      right,
      bottom,
      left,
    ])
  end

  def print_acc(acc, hide \\ 0) do
    acc
    |> Enum.map(fn line ->
      line
      |> Enum.map(fn val ->
        case val do
          ^hide -> "   "
          _ -> String.pad_leading("#{val}", 3)
        end
      end)
      |> Enum.join("")
    end)
    |> Enum.join("\n")
    |> IO.puts()

    IO.puts("==============================")

    acc
  end

end

AoC.part_1("example.txt")
AoC.part_1("input.txt")
AoC.part_2("example.txt")
AoC.part_2("input.txt")

defmodule AoC do

  def parse_input(filename) do
    filename
    |> File.stream!()
    |> Stream.map(fn line -> String.split(line, [" ", " | ", "\n"], trim: true) end)
  end

  def part_1(filename) do
    count = parse_input(filename)
    |> Enum.flat_map(fn line -> Enum.drop(line, 10) end)
    |> Enum.count(fn signal -> String.length(signal) in [2,3,4,7] end)

    IO.puts("day_8 - part_1 - #{filename} - #{count}")
  end

  def part_2(filename) do
    count = parse_input(filename)
    |> Enum.map(&solve/1)
    # |> IO.inspect()
    |> Enum.sum()

    IO.puts("day_8 - part_2 - #{filename} - #{count}")
  end

  def solve(line) do
    sorted = line
    |> Enum.map(fn signals ->
      signals
      |> String.split("")
      |> Enum.sort()
      |> Enum.join("")
    end)

    input = sorted
    |> Enum.take(10)
    |> Enum.group_by(&String.length/1)
    # |> IO.inspect()

    d1 = input |> Map.get(2) |> List.first()
    d4 = input |> Map.get(4) |> List.first()
    d7 = input |> Map.get(3) |> List.first()
    d8 = input |> Map.get(7) |> List.first()

    d9 = input |> Map.get(6) |> Enum.find(&includes(&1, d4))
    d0 = input |> Map.get(6) |> Enum.reject(&(&1 == d9)) |> Enum.find(&includes(&1, d7))
    d6 = input |> Map.get(6) |> Enum.reject(&(&1 in [d0, d9])) |> List.first()

    d3 = input |> Map.get(5) |> Enum.find(&includes(&1, d1))
    d5 = input |> Map.get(5) |> Enum.reject(&(&1 == d3)) |> Enum.find(&includes(d9, &1))
    d2 = input |> Map.get(5) |> Enum.reject(&(&1 in [d3, d5])) |> List.first()

    mapping = [
      {d0, 0},
      {d1, 1},
      {d2, 2},
      {d3, 3},
      {d4, 4},
      {d5, 5},
      {d6, 6},
      {d7, 7},
      {d8, 8},
      {d9, 9},
    ]
    |> Enum.into(%{})
    # |> IO.inspect()

    sorted
    |> Enum.drop(10)
    # |> IO.inspect()
    |> Enum.map(&Map.get(mapping, &1))
    |> Enum.join("")
    |> String.to_integer()
  end

  def includes(left, right) do
    right
    |> String.graphemes()
    |> Enum.all?(&String.contains?(left, &1))
  end

end

AoC.part_1("example.txt")
AoC.part_1("input.txt")
AoC.part_2("example.txt")
AoC.part_2("input.txt")

defmodule AoC do
  def read_lines(file), do: file |> File.stream!() |> Stream.map(&String.trim/1)

  def to_bit(bit, :epsilon) when bit > 0, do: ?1
  def to_bit(_, :epsilon), do: ?0
  def to_bit(bit, :gamma) when bit > 0, do: ?0
  def to_bit(_, :gamma), do: ?1

  def to_int(number) do
    {value, ""} = Integer.parse(number, 2)
    value
  end

  def to_number(bits, rate) do
    bits
    |> Enum.map(&to_bit(&1, rate))
    |> to_string()
    |> to_int()
  end

  def part_1(input) do
    bits = input
    |> read_lines()
    |> Stream.map(&String.to_charlist/1)
    |> Stream.map(fn bits ->
      Enum.map(bits, fn
        ?0 -> -1
        ?1 -> 1
      end)
    end)
    |> Enum.reduce(fn bits, acc -> Enum.zip_with([bits, acc], &Enum.sum/1) end)

    epsilon = to_number(bits, :epsilon)
    gamma = to_number(bits, :gamma)

    IO.puts("day_3 - part_1 - #{input} - #{epsilon * gamma}")
  end

  def keep(zeros, ones, :O2) do
    if length(zeros) > length(ones) do
      zeros
    else
      ones
    end
  end
  def keep(zeros, ones, :CO2) do
    if length(zeros) > length(ones) do
      ones
    else
      zeros
    end
  end

  def step(numbers, pos, rating) do
    %{
      "0" => zeros,
      "1" => ones,
    } = Enum.group_by(numbers, &String.at(&1, pos))

    keep(zeros, ones, rating)
  end

  def find_number([number], _, _), do: to_int(number)
  def find_number(numbers, pos, rating) do
    next = step(numbers, pos, rating)
    find_number(next, pos + 1, rating)
  end

  def part_2(input) do
    numbers = read_lines(input)

    rating_o2 = find_number(numbers, 0, :O2)
    rating_co2 = find_number(numbers, 0, :CO2)

    IO.puts("day_3 - part_2 - #{input} - #{rating_o2 * rating_co2}")
  end
end

AoC.part_1("example.txt")
AoC.part_1("input.txt")
AoC.part_2("example.txt")
AoC.part_2("input.txt")

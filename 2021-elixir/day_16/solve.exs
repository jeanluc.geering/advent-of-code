defmodule Bits do
  def as_string(binary, joiner \\ "") do
    for(<<x::size(1) <- binary>>, do: "#{x}")
    |> Enum.chunk_every(8)
    |> Enum.join(joiner)
  end
end

# Mix.install([
#   {:nimble_parsec, "~> 1.2"},
# ])

# defmodule Day16Parser do
#   import NimbleParsec

#   date =
#     integer(4)
#     |> ignore(string("-"))
#     |> integer(2)
#     |> ignore(string("-"))
#     |> integer(2)

#   time =
#     integer(2)
#     |> ignore(string(":"))
#     |> integer(2)
#     |> ignore(string(":"))
#     |> integer(2)
#     |> optional(string("Z"))

#   defparsec :datetime, date |> ignore(string("T")) |> concat(time)
# end

defmodule AoC do

  defp parse_input(filename, line) do
    filename
    |> File.read!()
    |> String.split("\n", trim: true)
    |> Enum.drop(line - 1)
    |> List.first()
  end

  def part_1(filename, line) do
    {packet, _} = filename
    |> parse_input(line)
    # |> IO.inspect()
    |> Base.decode16!()
    # |> tap(fn data ->
    #   data
    #   |> Bits.as_string(" ")
    #   |> IO.inspect()
    # end)
    |> read_packet()
    # |> IO.inspect()

    result = sum_versions(packet)

    IO.puts("day_16 - part_1 - #{filename}:#{line} - #{result}")
  end

  defp read_packets(""), do: []
  defp read_packets(stream) do
    {packet, rest} = read_packet(stream)
    packets = read_packets(rest)
    [packet|packets]
  end

  defp read_packets(0, stream), do: {[], stream}
  defp read_packets(count, stream) do
    {packet, stream} = read_packet(stream)
    {packets, stream} = read_packets(count - 1, stream)
    {[packet|packets], stream}
  end

  defp read_packet(<<version::3, type::3, stream::bits>>) do
    {data, rest} = case type do
      4 -> read_litteral(stream)
      _ -> read_operator(stream)
    end
    packet = %{
      type: type,
      version: version,
      data: data,
    }
    {packet, rest}
  end

  defp pad_leading_zeros(bs) when is_binary(bs), do: bs
  defp pad_leading_zeros(bs) when is_bitstring(bs) do
    pad_length = 8 - rem(bit_size(bs), 8)
    <<0::size(pad_length), bs::bitstring>>
  end

  defp read_litteral(stream) do
    {bitstring, rest} = read_litteral_bytes(stream)
    data = bitstring |> pad_leading_zeros() |> :binary.decode_unsigned()
    {data, rest}
  end

  defp read_litteral_bytes(<<0::1, byte::4, stream::bits>>) do
    {<<byte::4>>, stream}
  end
  defp read_litteral_bytes(<<1::1, byte::4, stream::bits>>) do
    {bytes, rest} = read_litteral_bytes(stream)
    {<<byte::4, bytes::bits>>, rest}
  end

  defp read_operator(<<0::1, length::15, stream::bits>>) do
    <<data::bits-size(length), rest::bits>> = stream
    {read_packets(data), rest}
  end

  defp read_operator(<<1::1, count::11, stream::bits>>) do
    read_packets(count, stream)
  end

  defp sum_versions(packets) when is_list(packets), do: packets |> Enum.map(&sum_versions/1) |> Enum.sum()

  defp sum_versions(%{type: 4, version: version}), do: version
  defp sum_versions(%{version: version, data: packets}) when is_list(packets), do: version + sum_versions(packets)

  def part_2(filename, line) do
    {packet, _} = filename
    |> parse_input(line)
    |> Base.decode16!()
    |> read_packet()
    # |> IO.inspect()

    result = eval(packet)

    IO.puts("day_16 - part_2 - #{filename}:#{line} - #{result}")
  end

  defp eval(%{type: 0, data: payload}),
    do: payload |> Enum.map(&eval/1) |> Enum.sum()
  defp eval(%{type: 1, data: payload}),
    do: payload |> Enum.map(&eval/1) |> Enum.product()
  defp eval(%{type: 2, data: payload}),
    do: payload |> Enum.map(&eval/1) |> Enum.min()
  defp eval(%{type: 3, data: payload}),
    do: payload |> Enum.map(&eval/1) |> Enum.max()
  defp eval(%{type: 4, data: number}),
    do: number
  defp eval(%{type: type, data: data}) do
    [left, right] = data |> Enum.map(&eval/1)
    case type do
      5 -> if left > right, do: 1, else: 0
      6 -> if left < right, do: 1, else: 0
      7 -> if left == right, do: 1, else: 0
    end
  end

end

Enum.each(1..7, &AoC.part_1("example.txt", &1))
AoC.part_1("input.txt", 1)
AoC.part_2("example.txt", 8)
AoC.part_2("example.txt", 9)
AoC.part_2("example.txt", 10)
AoC.part_2("example.txt", 11)
AoC.part_2("input.txt", 1)

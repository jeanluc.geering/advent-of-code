defmodule AoC do

  defp solve(filename, iterations) do
    {[template], rules} = filename
    |> File.read!()
    |> String.trim("\n")
    |> String.split("\n")
    |> Enum.split_while(&(&1 != ""))

    polymer = template
    |> String.graphemes()
    |> Enum.chunk_every(2, 1, :discard)
    |> Enum.map(&List.to_tuple/1)
    |> Enum.frequencies()

    rules = rules
    |> Enum.drop(1)
    |> Enum.map(fn line ->
      [pattern, char] = String.split(line, " -> ")

      pattern = pattern
      |> String.graphemes()
      |> List.to_tuple()

      {pattern, char}
    end)
    |> Enum.into(%{})

    result =  1..iterations
    |> Enum.reduce(polymer, fn i, acc -> step(i, acc, rules) end)
    |> count_elements(template)
    # |> IO.inspect()
    |> max_min_diff()

    IO.puts("day_14 - #{iterations} iterations - #{filename} - #{result}")
  end

  defp step(_i, polymer, rules) do
    # IO.puts(i)

    polymer
    |> Enum.reduce(polymer, fn
      {_, 0}, acc -> acc
      {{l,r} = pair, count}, acc ->
        case Map.get(rules, pair) do
          nil -> acc
          c ->
            acc
            |> Map.update!({l,r}, &(&1 - count))
            |> Map.update({l,c}, count, &(&1 + count))
            |> Map.update({c,r}, count, &(&1 + count))
        end
    end)
  end

  def count_elements(polymer, template) do
    first_element = String.slice(template, 0, 1)
    last_element = String.slice(template, -1, 1)

    polymer
    # |> IO.inspect()
    |> Enum.reduce(%{}, fn
      {_, 0}, acc -> acc
      {{l,r}, count}, acc ->
        acc
        |> Map.update(l, count, &(&1 + count))
        |> Map.update(r, count, &(&1 + count))
    end)
    |> Map.update!(first_element, &(&1 + 1))
    |> Map.update!(last_element, &(&1 + 1))
    # |> IO.inspect()
    |> Enum.map(fn {element, count} -> {element, div(count, 2)} end)
  end

  defp max_min_diff(frequencies) do
    {min, max} = frequencies
    |> Enum.into(%{})
    |> Map.values()
    |> Enum.min_max()

    max - min
  end

  def part_1(filename), do: solve(filename, 10)
  def part_2(filename), do: solve(filename, 40)

end

AoC.part_1("example.txt")
AoC.part_1("input.txt")
AoC.part_2("example.txt")
AoC.part_2("input.txt")

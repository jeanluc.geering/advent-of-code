defmodule AoC do

  def parse_input(filename) do
    lines = File.read!(filename) |> String.split("\n", trim: true)

    lines
    |> Enum.map(fn line ->
      [^line, x1, y1, x2, y2] = Regex.run(~r/(\d+),(\d+) -> (\d+),(\d+)/, line)
      {
        String.to_integer(x1),
        String.to_integer(y1),
        String.to_integer(x2),
        String.to_integer(y2)
      }
    end)
  end

  def part_1(filename, size) do
    lines = parse_input(filename)

    {x_lines, y_lines} = lines
    |> Enum.map(fn
      {x1, y1, x2, y2} when x1 == x2 and y2 < y1 ->
        {:x, x1, y2, y1}
      {x1, y1, x2, y2} when x1 == x2 ->
        {:x, x1, y1, y2}
      {x1, y1, x2, y2} when y1 == y2 and x2 < x1 ->
        {:y, y1, x2, x1}
      {x1, y1, x2, y2} when y1 == y2 ->
        {:y, y1, x1, x2}
      _ ->
        nil
    end)
    |> Enum.reject(&is_nil/1)
    |> Enum.split_with(fn
      {:x, _, _, _} -> true
      _ -> false
    end)
    # |> IO.inspect()

    grid = Enum.reduce(x_lines, new_grid(size), fn {:x, x, from, to}, grid ->
      update_in(grid, [Access.at(x)], fn row ->
        row
        |> Enum.with_index()
        |> Enum.map(fn
          {value, index} when from <= index and index <= to -> value + 1
          {value, _} -> value
        end)
      end)
    end)

    grid = Enum.reduce(y_lines, transpose(grid), fn {:y, x, from, to}, grid ->
      update_in(grid, [Access.at(x)], fn row ->
        row
        |> Enum.with_index()
        |> Enum.map(fn
          {value, index} when from <= index and index <= to -> value + 1
          {value, _} -> value
        end)
      end)
    end)

    # print_grid(grid)

    count = grid
    |> List.flatten()
    |> Enum.filter(&(&1 > 1))
    |> length()

    IO.puts("day_5 - part_1 - #{filename} - #{count}")
  end

  def transpose(rows) do
    rows
    |> List.zip
    |> Enum.map(&Tuple.to_list/1)
  end

  def new_grid(size) do
    for _ <- 1..size do
      for _ <- 1..size, do: 0
    end
  end

  def print_grid(grid) do
    grid
    |> Enum.map(fn row ->
      data = Enum.map(row, fn
        0 -> "·"
        i -> i+48
        # 0 -> " ·"
        # i -> [" ", i+48]
      end)
      [data|"\n"]
    end)
    |> IO.puts()

    grid
  end

  def part_2(filename, size) do
    lines = parse_input(filename)

    lines = lines
    |> Enum.map(fn
      {x1, y1, x2, y2} when x1 == x2 and y2 < y1 ->
        {:x, x1, y2, y1}
      {x1, y1, x2, y2} when x1 == x2 ->
        {:x, x1, y1, y2}
      {x1, y1, x2, y2} when y1 == y2 and x2 < x1 ->
        {:y, y1, x2, x1}
      {x1, y1, x2, y2} when y1 == y2 ->
        {:y, y1, x1, x2}
      {x1, y1, x2, y2} when x2-x1 == y2-y1 and x2 < x1 ->
        {:d1, x2, y2, x1-x2}
      {x1, y1, x2, y2} when x2-x1 == y2-y1 ->
        {:d1, x1, y1, x2-x1}
      {x1, y1, x2, y2} when x2-x1 == -1 * y2-y1 and x2 < x1 ->
        {:d2, x2, y2, x1-x2}
      {x1, y1, x2, y2} when x2-x1 == -1 * (y2-y1) ->
        {:d2, x1, y1, x2-x1}
    end)
    # |> IO.inspect()
    |> Enum.reject(&is_nil/1)
    |> Enum.group_by(fn {dir, _, _, _} -> dir end)
    # |> IO.inspect()

    grid = lines
    |> Map.get(:x)
    |> Enum.reduce(new_grid(size), fn {:x, x, from, to}, grid ->
      update_in(grid, [Access.at(x)], fn row ->
        row
        |> Enum.with_index()
        |> Enum.map(fn
          {value, index} when from <= index and index <= to -> value + 1
          {value, _} -> value
        end)
      end)
    end)

    grid = lines
    |> Map.get(:d1)
    |> Enum.reduce(grid, fn {:d1, x, y, length}, grid ->
      Enum.reduce(0..length, grid, fn i, grid ->
        update_in(grid, [Access.at(x+i), Access.at(y+i)], fn value -> value + 1 end)
      end)
    end)

    grid = lines
    |> Map.get(:d2)
    |> Enum.reduce(grid, fn {:d2, x, y, length}, grid ->
      Enum.reduce(0..length, grid, fn i, grid ->
        update_in(grid, [Access.at(x+i), Access.at(y-i)], fn value -> value + 1 end)
      end)
    end)

    grid = lines
    |> Map.get(:y)
    |> Enum.reduce(transpose(grid), fn {:y, x, from, to}, grid ->
      update_in(grid, [Access.at(x)], fn row ->
        row
        |> Enum.with_index()
        |> Enum.map(fn
          {value, index} when from <= index and index <= to -> value + 1
          {value, _} -> value
        end)
      end)
    end)

    # print_grid(grid)

    count = grid
    |> List.flatten()
    |> Enum.filter(&(&1 > 1))
    |> length()

    IO.puts("day_5 - part_2 - #{filename} - #{count}")
  end

end

AoC.part_1("example.txt", 10)
AoC.part_1("input.txt", 1000)
AoC.part_2("example.txt", 10)
AoC.part_2("input.txt", 1000)

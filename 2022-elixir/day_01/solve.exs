defmodule AoC do
	def read_lines(file), do: file |> File.stream!() |> Stream.map(&String.trim/1)

	def part_1(input) do
		max = input
		|> read_lines()
		|> Stream.map(fn
			"" -> nil
			s -> String.to_integer(s)
		end)
		|> Enum.reduce({0, 0}, fn
			nil, max_acc -> {max(max_acc), 0}
			value, {max, acc} -> {max, acc + value}
		end)
		|> max()

		IO.puts("day_1 - part_1 - #{input} - #{max}")
	end

	defp max({a, b}) when b > a, do: b
	defp max({a, _}), do: a

	def part_2(input) do
		sum = input
		|> read_lines()
		|> Stream.map(fn
			"" -> nil
			s -> String.to_integer(s)
		end)
		|> Enum.reduce({[], 0}, fn
			nil, acc_and_cal -> {concat(acc_and_cal), 0}
			value, {acc, calories} -> {acc, calories + value}
		end)
		|> concat()
		|> Enum.sort()
		|> Enum.reverse()
		|> Enum.take(3)
		|> Enum.sum()

		IO.puts("day_1 - part_2 - #{input} - #{sum}")
	end

	defp concat({list, item}), do: [item | list]
 end

#  AoC.part_1("example.txt")
#  AoC.part_1("input.txt")
 AoC.part_2("example.txt")
 AoC.part_2("input.txt")

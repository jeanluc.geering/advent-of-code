defmodule AoC do
	def read_lines(file), do: file |> File.stream!() |> Stream.map(&String.trim/1)

	def part_1(input) do
		score = input
		|> read_lines()
		|> Stream.map(&parse_line/1)
		|> Stream.map(&score_1/1)
		|> Enum.sum()

		IO.puts("day_2 - part_1 - #{input} - #{score}")
	end

	defp parse_line(line) do
		line
		|> String.split()
		|> Enum.map(fn
			"A" -> 0
			"B" -> 1
			"C" -> 2
			"X" -> 0
			"Y" -> 1
			"Z" -> 2
		end)
	end

	defp score_1([l, r]) do
		shape_score = r + 1

		outcome = case {l, r} do
			{x, x} -> 3
			{l, r} when rem(l + 1, 3) == r -> 6
			_ -> 0
		end

		shape_score + outcome
	end

	def part_2(input) do
		score = input
		|> read_lines()
		|> Stream.map(&parse_line/1)
		|> Stream.map(&score_2/1)
		|> Enum.sum()

		IO.puts("day_2 - part_2 - #{input} - #{score}")
	end

	defp score_2([l, r]) do
		shape = case r do
			0 -> rem(l + 2, 3)
			1 -> l
			2 -> rem(l + 1, 3)
		end

		shape_score = shape + 1

		outcome = r * 3

		shape_score + outcome
	end
 end

 AoC.part_1("example.txt")
 AoC.part_1("input.txt")
 AoC.part_2("example.txt")
 AoC.part_2("input.txt")

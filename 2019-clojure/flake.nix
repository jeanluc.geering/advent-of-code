{
  description = "Flake to manage elixir project";

  inputs = {
    # nixpkgs.url = "github:NixOS/nixpkgs/master";
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let pkgs = nixpkgs.legacyPackages.${system};
      in {
        # see https://nixos.org/manual/nixpkgs/unstable/#sec-pkgs-mkShell
        # todo review https://nixos.org/manual/nixpkgs/unstable/#sec-beam
        # TODO look into https://github.com/numtide/devshell as an alternative
        # doc: https://numtide.github.io/devshell/intro.html
        # => e.g. doesn't import all env vars required for building a C project
        devShell = pkgs.mkShell {
          packages = [
            pkgs.jdk17_headless
            pkgs.clojure
            pkgs.babashka
          ];
          shellHook = ''
            echo "JAVA: $(java --version)"
            echo "CLOJURE: $(clojure --version)"
            echo "BABASHKA: $(bb --version)"
          '';
        };
      });
}

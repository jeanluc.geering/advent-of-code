(ns day05
  (:require
   [clojure.test :as t]
   [clojure.string :as str]))

; TODO how to handle IO

; parse opcode(value) => op/fn, maybe size, [param type, value]*, return address?
; load(memory, pointer) = parse opcode / load params => instr (opcode/fn, data, pos, size)
; next_inp(input, instr) = input
; next_out(output, instr) = output
; next_mem(memory, instr) = memory
; next(state) = load / exec => state

; state: %{memory, input, output, instructions/history, pointer}

(defn- digits [n]
  (->> n (format "%05d") (map (comp read-string str))))


(t/with-test
  (defn- parse-opcode [value]
    (let [[m3 m2 m1 o1 o2] (digits value)
          opcode (+ (* o1 10) o2)]
      {:opcode opcode, :modes [m1 m2 m3]}))

  (t/is (= (parse-opcode 1) {:opcode 1, :modes [0 0 0]}))
  (t/is (= (parse-opcode 1002) {:opcode 2, :modes [0 1 0]}))
  (t/is (= (parse-opcode 103) {:opcode 3, :modes [1 0 0]}))
  (t/is (= (parse-opcode 99) {:opcode 99, :modes [0 0 0]})))

(defn- load-param [values, modes, index, memory]
  (let [mode (nth modes index nil)
        value (nth values index nil)]
    (case mode
      0 (nth memory value nil)
      1 value)))

(t/with-test
  (defn- load [{memory :memory, input :input, pointer :pointer}]
    (let [{opcode :opcode, modes :modes} (parse-opcode (nth memory pointer))
          values (drop (inc pointer) memory)
          p1 (load-param values modes 0 memory)]
      (case opcode
        1 {:opcode opcode :value (+ p1 (load-param values modes 1 memory)) :pos (nth values 2)}
        2 {:opcode opcode :value (* p1 (load-param values modes 1 memory)) :pos (nth values 2)}
        3 {:opcode opcode :value (first input) :pos (first values)}
        4 {:opcode opcode :value p1})))

  (let [state {:memory []
               :input []
               :output []
               :pointer 0}]

    (t/is (= (load (assoc state :memory [1101 0 0 0])) {:opcode 1 :value 0 :pos 0}))
    (t/is (= (load (assoc state :memory [1101 100 -1 4 0])) {:opcode 1 :value 99 :pos 4}))
    (t/is (= (load (assoc state :memory [1 0 0 0 0])) {:opcode 1 :value 2 :pos 0}))
    (t/is (= (load (assoc state :memory [2 0 0 0 0])) {:opcode 2 :value 4 :pos 0}))
    (t/is (= (load (assoc state :memory [3 0] :input [5])) {:opcode 3 :value 5 :pos 0}))
    (t/is (= (load (assoc state :memory [104 15 16])) {:opcode 4 :value 15}))
    (t/is (= (load (assoc state :memory [4 0])) {:opcode 4 :value 4}))))


(t/with-test
  (defn- next-input [input {opcode :opcode}]
    (case opcode
      3 (rest input)
      input))
  (t/is (= (next-input [1 2 3] {:opcode 1}) [1 2 3]))
  (t/is (= (next-input [1 2 3] {:opcode 3}) [2 3])))

(t/with-test
  (defn- next-output [output {opcode :opcode, value :value}]
    (case opcode
      4 (conj output value)
      output))
  (t/is (= (next-output [1 2 3] {:opcode 1 :value 4}) [1 2 3]))
  (t/is (= (next-output [1 2 3] {:opcode 4 :value 4}) [1 2 3 4])))

(t/with-test
  (defn- next-memory [memory {opcode :opcode, value :value, pos :pos}]
    (case opcode
      1 (assoc memory pos value)
      2 (assoc memory pos value)
      3 (assoc memory pos value)
      memory))
  (t/is (= (next-memory [1 2 3] {:opcode 1 :value 4 :pos 1}) [1 4 3]))
  (t/is (= (next-memory [1 2 3] {:opcode 2 :value 4 :pos 1}) [1 4 3]))
  (t/is (= (next-memory [1 2 3] {:opcode 3 :value 4 :pos 1}) [1 4 3]))
  (t/is (= (next-memory [1 2 3] {:opcode 4 :value 4}) [1 2 3])))

(t/with-test
  (defn- next-pointer [pointer {opcode :opcode}]
    (let [size (case opcode 1 4 2 4 3 2 4 2)]
      (+ pointer size)))
  (t/is (= (next-pointer 1 {:opcode 1}) 5))
  (t/is (= (next-pointer 1 {:opcode 3}) 3)))

(t/with-test
  (defn- next [state]
    (let [instr (load state)]
      (assoc state
             :memory (next-memory (:memory state) instr)
             :input (next-input (:input state) instr)
             :output (next-output (:output state) instr)
             :pointer (next-pointer (:pointer state) instr)
             :log (conj (:log state) instr))))

  (let [state {:memory [99]
               :input []
               :output []
               :pointer 0
               :log []}]

    (t/is (= (next (assoc state :memory [3 0 99] :input [5])) {:memory [5 0 99]
                                                            :input []
                                                            :output []
                                                            :pointer 2
                                                            :log [{:opcode 3 :value 5 :pos 0}]}))
    #_(t/is (= (next state) state))))


(t/run-tests)

(def program
  (as-> "data/day05.txt" v
    (slurp v)
    (str/trim-newline v)
    (str/split v #",")
    (map #(Integer/parseInt %) v)
    (vec v)))


(defn- do-run [state]
  (let [memory (:memory state)
        pointer (:pointer state)
        opcode (nth memory pointer)]
    (case opcode
      99 state
      (recur (next state)))
    ))

(defn run [data input]
  (let [state {:memory data
               :input [input]
               :output []
               :pointer 0
               :log []}]
    (do-run state))
  )

(comment
  (run [99] nil)
  (run [3 0 99] 1)
  (run program 1)
  )

(def part1
  (let [result (run program 1)
        output (:output result)
        diag (last output)]
    diag))

(println "DAY 5 - PART 1 -" part1)

:done

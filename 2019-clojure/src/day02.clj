(ns day02
  (:require
   [clojure.string :as str]))

(def input
  (as-> "data/day02.txt" v
    (slurp v)
    (str/trim-newline v)
    (str/split v #",")
    (map #(Integer/parseInt %) v)
    (vec v)))

(defn program [noun verb]
  (-> input
      (assoc 1 noun)
      (assoc 2 verb)))

(comment
  input
  (get input 0))

(defn instruction [state i]
  (-> i
      (* 4)
      (drop state)))

(comment
  (instruction input 1))

(defn calc [state, op, a1, a2, a3]
  (let [v1 (nth state a1)
        v2 (nth state a2)
        sum (op v1 v2)]
    (assoc state a3 sum)))

(comment
  (calc input + 0 0 3))

(defn step [state instruction-pointer]
  (let [[opcode, p1, p2, p3] (instruction state instruction-pointer)]
    (case opcode
      1 (calc state + p1 p2 p3)
      2 (calc state * p1 p2 p3)
      99 (reduced (nth state 0)))))

(comment
  (take 12 input)
  (take 12 (step input 0)))

(defn run [noun verb]
  (let [state (program noun verb)]
    (reduce #(step %1 %2) state (range))))

(def part1 (run 12 2))

(println "DAY 2 - PART 1 -" part1)

(def part2
  (for [noun (range 100)
        verb (range 100)
        :let [output (run noun verb)]
        :when (= output 19690720)]
    (+ (* noun 100) verb)))

(println "DAY 2 - PART 2 -" part2)

:done

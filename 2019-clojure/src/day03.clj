(ns day03
  (:require
   [clojure.set :as set]
   [clojure.string :as str]))

(defn parse-move [move]
  (let [direction (subs move 0 1)
        distance (subs move 1)]
    [direction (Integer/parseInt distance)]))

(defn parse-path [line]
  (as-> line v
    (str/split v #",")
    (map parse-move v)))

(def input
  (as-> "data/day03.txt" v
    (slurp v)
    (str/trim-newline v)
    (str/split-lines v)
    (map parse-path v)))

(defn step [direction]
  (case direction
    "U" [0  1]
    "D" [0 -1]
    "R" [1  0]
    "L" [-1  0]))

(comment
  (step "U"))

(defn line [start [direction distance]]
  (let [step (step direction)
        points (reductions #(mapv + %1 %2) start (repeat distance step))
        new-points (drop 1 points)
        end (last new-points)]
    [new-points end]))

(comment
  (line [4 0] ["D" 2]))

(defn wire
  ([steps]
   (wire [] [0 0] steps))
  ([points origin steps]
   (if (seq steps)
     (let [step (first steps)
           [new-points end] (line origin step)]
       (recur (concat points new-points) end (next steps)))
     points)))

(comment
  (wire [["R" 4] ["D" 2]]))

(def wire1 (wire (nth input 0)))
(def wire2 (wire (nth input 1)))
(def intersections (set/intersection (set wire1) (set wire2)))

(def part1
  (let [distances (map #(reduce + (map abs %1)) intersections)]
    (first (sort distances))))

(println "DAY 3 - PART 1 -" part1)

(def part2
  (let [d1 (into {}
                 (keep-indexed #(when (intersections %2) [%2 (+ %1 1)]) wire1))
        d2 (into {} (keep-indexed #(when (intersections %2) [%2 (+ %1 1)]) wire2))]
    (->> intersections
         (map #(+ (d1 %1) (d2 %1)))
         (sort)
         (first))))

(println "DAY 3 - PART 2 -" part2)

:done

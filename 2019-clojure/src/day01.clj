(ns day01
  (:require
   [clojure.string :as str]))

(defn fuel [mass]
  (-> mass
      (quot 3)
      (- 2)))

(comment
  (fuel 12)
  (fuel 14)
  (fuel 1969)
  (fuel 100756))

(defn total-fuel-requirements [modules]
  (->> modules
       (map fuel)
       (reduce +)))

(comment
  (total-fuel-requirements '(12 14)))

(def input (slurp "data/day01.txt"))

(def part1
  (->> input
       (str/split-lines)
       (map #(Integer/parseInt %))
       (total-fuel-requirements)))

(println "DAY 1 - PART 1 -" part1)

(defn more-fuel [mass]
  (let [f (fuel mass)]
    (cond
      (< f 1) 0
      :else (+ f (more-fuel f)))))

(comment
  (more-fuel 1969))

(def part2
  (->> input
       (str/split-lines)
       (map #(Integer/parseInt %))
       (map more-fuel)
       (reduce +)))

(println "DAY 1 - PART 2 -" part2)

:done

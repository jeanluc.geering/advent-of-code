(ns day04)

(defn digits [n]
  (->> n str (map (comp read-string str))))

(def input (range 245318 765747))

(comment
  (map digits input))

(defn adjacent-digits [[d1 d2 d3 d4 d5 d6]]
  (or
   (= d1 d2)
   (= d2 d3)
   (= d3 d4)
   (= d4 d5)
   (= d5 d6)))

(comment
  (adjacent-digits '(1 2 3 4 5 6))
  (adjacent-digits '(2 1 3 4 5 6))
  (adjacent-digits '(1 1 3 4 5 6))
  (adjacent-digits '(1 1 1 1 1 1))
  (adjacent-digits '(1 1 1 9 1 1))
  (adjacent-digits '(1 1 1 0 1 1))
  (adjacent-digits '(1 2 3 3 3 3)))

(defn never-decrease [[d1 d2 d3 d4 d5 d6]]
  (and
   (<= d1 d2)
   (<= d2 d3)
   (<= d3 d4)
   (<= d4 d5)
   (<= d5 d6)))

(comment
  (never-decrease '(1 2 3 4 5 6))
  (never-decrease '(2 1 3 4 5 6))
  (never-decrease '(1 1 3 4 5 6))
  (never-decrease '(1 1 1 1 1 1))
  (never-decrease '(1 1 1 9 1 1))
  (never-decrease '(1 1 1 0 1 1))
  )

(def part1
  (->> input
       (map digits)
       (filter adjacent-digits)
       (filter never-decrease)
       (count)))

(println "DAY 4 - PART 1 -" part1)

(defn adjacent-digits-2 [[d1 d2 d3 d4 d5 d6]]
  (or
   (and (= d1 d2) (not= d2 d3))
   (and (= d2 d3) (not= d1 d2) (not= d3 d4))
   (and (= d3 d4) (not= d2 d3) (not= d4 d5))
   (and (= d4 d5) (not= d3 d4) (not= d5 d6))
   (and (= d5 d6) (not= d4 d5))))

(comment
  (adjacent-digits-2 '(1 1 2 2 3 3))
  (adjacent-digits-2 '(1 2 3 4 4 4))
  (adjacent-digits-2 '(1 2 2 2 3 4))
  (adjacent-digits-2 '(1 1 1 1 2 2)))

(def part2
  (->> input
       (map digits)
       (filter adjacent-digits-2)
       (filter never-decrease)
       (count)))

(println "DAY 4 - PART 2 -" part2)
